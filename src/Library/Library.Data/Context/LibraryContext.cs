﻿using Library.Data.Dao;

namespace Library.Data.Context
{
    using Microsoft.EntityFrameworkCore;

    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> options)
            :base(options)
        {
        }

        public DbSet<LibrarianEntity> Librarians { get; set; }

        public DbSet<SpecimenEntity> Specimens { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("LibraryDB");
        }
    }
}
