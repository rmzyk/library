﻿using System;

namespace Library.Domain
{
    public class Loan
    {
        public Guid Id { get; private set; }

        public DateTime BorrowDate { get; private set; }

        public DateTime GiveBackDate { get; private set; }

    }
}
