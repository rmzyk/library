﻿namespace Library.Domain.Enums
{
    public enum SpecimenState
    {
        InLibrary,
        Loaned,
        Lost
    }
}
