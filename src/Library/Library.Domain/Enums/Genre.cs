﻿namespace Library.Domain.Enums
{
    public enum Genre
    {
        Poetry,
        Fiction,
        Nonfiction
    }
}
