﻿namespace Library.Domain
{
    using System;
    using System.Collections.Generic;
    using Library.Domain.Enums;

    public class Librarian
    {
        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public string Surname { get; private set; }

        public Address Address { get; private set; }

        public List<Specimen> Specimens { get; private set; }


        public Librarian()
        {

        }

        public static Librarian AddNewLibrarian(string name, string surname, Address address)
            => new Librarian(name, surname, address);

        public void BorrowBook(Specimen specimen)
        {
            if (specimen.State is SpecimenState.InLibrary && this.Specimens.Count < 5)
                specimen.Borrow(this);
            this.Specimens.Add(specimen);
        }

        public void GiveBookBack(Specimen specimen)
        {
            if (specimen.Librarian.Id != this.Id) return;
            specimen.GiveBack(this);
            this.Specimens.Remove(specimen);
        }

        private Librarian(string name, string surname, Address address)
        {
            this.Id = Guid.NewGuid();
            this.SetName(name);
            this.SetSurname(surname);
            this.SetAddress(address);
            this.Specimens = new List<Specimen>();
        }

        private void SetName(string name)
        {
            if (!string.IsNullOrWhiteSpace(name) && name.Length < 20) this.Name = name;
        }

        private void SetSurname(string surname)
        {
            if (!string.IsNullOrWhiteSpace(surname) && surname.Length < 20) this.Surname = surname;
        }

        private void SetAddress(Address address)
        {
            if (address.Id != Guid.Empty && !string.IsNullOrWhiteSpace(address.City) &&
                !string.IsNullOrWhiteSpace(address.Street))
                this.Address = address;
        }
    }
}
