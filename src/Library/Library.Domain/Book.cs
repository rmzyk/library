﻿using System;
using System.Collections.Generic;
using Library.Domain.Enums;

namespace Library.Domain
{
    public class Book
    {
        public Guid Id { get; private set; }

        public Author Author { get; private set; }

        public string Title { get; private set; }

        public Genre Genre { get; private set; }

        public List<Specimen> Specimens { get; private set; }

        public Book()
        {
            
        }

        private Book(string Author, string Title, Genre genre)
        {
            
        }

        private setAuthor
    }
}
