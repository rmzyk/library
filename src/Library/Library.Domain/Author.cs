﻿using System;
using System.Collections.Generic;

namespace Library.Domain
{
    public class Author
    {
        public Guid Id { get; private set; }

        public List<Book> Books { get; private set; }

        //TODO: extend publisher domain object
    }
}
