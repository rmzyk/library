﻿using System.Collections.Generic;

namespace Library.Domain
{
    using System;
    using Library.Domain.Enums;

    //TODO: Error handling
    public class Specimen
    {
        public Guid Id { get; private set; }

        public DateTime PublishDate { get; private set; }

        public DateTime? BorrowDate { get; private set; }

        public DateTime? GiveBackDate { get; private set; }

        public Publisher Publisher { get; private set; }

        public Librarian Librarian { get; private set; }

        public SpecimenState State { get; private set; }

        public List<Loan>

        public Specimen()
        {

        }

        private Specimen(Publisher publisher, DateTime publishDate, SpecimenState state)
        {
            this.Publisher = publisher;
            this.PublishDate = publishDate;
            this.State = state;
        }

        public void Borrow(Librarian librarian)
        {
            if (librarian.Id == Guid.Empty || !(this.State is SpecimenState.InLibrary) ||
                !(this.Librarian is null)) return;
            this.Librarian = librarian;
            this.State = SpecimenState.Loaned;
            this.BorrowDate = DateTime.Now;
        }

        public void GiveBack(Librarian librarian)
        {
            if (!(this.State is SpecimenState.Loaned && this.Librarian.Id == librarian.Id)) return;
            this.State = SpecimenState.InLibrary;
            this.Librarian = null;
            this.GiveBackDate = DateTime.Now;
        }
    }
}
