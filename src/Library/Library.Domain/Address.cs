﻿using System;

namespace Library.Domain
{
   public struct Address
    {
        public Guid Id { get; private set; }

        public string City { get; private set; }

        public string Street { get; private set; }

        //TODO: extend publisher domain object
    }
}
